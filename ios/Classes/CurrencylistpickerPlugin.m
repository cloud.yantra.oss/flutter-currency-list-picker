#import "CurrencylistpickerPlugin.h"
#if __has_include(<currencylistpicker/currencylistpicker-Swift.h>)
#import <currencylistpicker/currencylistpicker-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "currencylistpicker-Swift.h"
#endif

@implementation CurrencylistpickerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftCurrencylistpickerPlugin registerWithRegistrar:registrar];
}
@end
