List<Map> codes_en = [
  {
    "code": "AED",
    "numeric_code": "784",
    "minor_unit": "2",
    "name": "United Arab Emirates dirham"
  },
  {
    "code": "AFN",
    "numeric_code": "971",
    "minor_unit": "2",
    "name": "Afghan afghani"
  },
  {
    "code": "ALL",
    "numeric_code": "8",
    "minor_unit": "2",
    "name": "Albanian lek"
  },
  {
    "code": "AMD",
    "numeric_code": "51",
    "minor_unit": "2",
    "name": "Armenian dram"
  },
  {
    "code": "ANG",
    "numeric_code": "532",
    "minor_unit": "2",
    "name": "Netherlands Antillean guilder"
  },
  {
    "code": "AOA",
    "numeric_code": "973",
    "minor_unit": "2",
    "name": "Angolan kwanza"
  },
  {
    "code": "ARS",
    "numeric_code": "32",
    "minor_unit": "2",
    "name": "Argentine peso"
  },
  {
    "code": "AUD",
    "numeric_code": "36",
    "minor_unit": "2",
    "name": "Australian dollar"
  },
  {
    "code": "AWG",
    "numeric_code": "533",
    "minor_unit": "2",
    "name": "Aruban florin"
  },
  {
    "code": "AZN",
    "numeric_code": "944",
    "minor_unit": "2",
    "name": "Azerbaijani manat"
  },
  {
    "code": "BAM",
    "numeric_code": "977",
    "minor_unit": "2",
    "name": "Bosnia and Herzegovina convertible mark"
  },
  {
    "code": "BBD",
    "numeric_code": "52",
    "minor_unit": "2",
    "name": "Barbados dollar"
  },
  {
    "code": "BDT",
    "numeric_code": "50",
    "minor_unit": "2",
    "name": "Bangladeshi taka"
  },
  {
    "code": "BGN",
    "numeric_code": "975",
    "minor_unit": "2",
    "name": "Bulgarian lev"
  },
  {
    "code": "BHD",
    "numeric_code": "48",
    "minor_unit": "3",
    "name": "Bahraini dinar"
  },
  {
    "code": "BIF",
    "numeric_code": "108",
    "minor_unit": "0",
    "name": "Burundian franc"
  },
  {
    "code": "BMD",
    "numeric_code": "60",
    "minor_unit": "2",
    "name": "Bermudian dollar"
  },
  {
    "code": "BND",
    "numeric_code": "96",
    "minor_unit": "2",
    "name": "Brunei dollar"
  },
  {"code": "BOB", "numeric_code": "68", "minor_unit": "2", "name": "Boliviano"},
  {
    "code": "BOV",
    "numeric_code": "984",
    "minor_unit": "2",
    "name": "Bolivian Mvdol (funds code)"
  },
  {
    "code": "BRL",
    "numeric_code": "986",
    "minor_unit": "2",
    "name": "Brazilian real"
  },
  {
    "code": "BSD",
    "numeric_code": "44",
    "minor_unit": "2",
    "name": "Bahamian dollar"
  },
  {
    "code": "BTN",
    "numeric_code": "64",
    "minor_unit": "2",
    "name": "Bhutanese ngultrum"
  },
  {
    "code": "BWP",
    "numeric_code": "72",
    "minor_unit": "2",
    "name": "Botswana pula"
  },
  {
    "code": "BYN",
    "numeric_code": "933",
    "minor_unit": "2",
    "name": "Belarusian ruble"
  },
  {
    "code": "BZD",
    "numeric_code": "84",
    "minor_unit": "2",
    "name": "Belize dollar"
  },
  {
    "code": "CAD",
    "numeric_code": "124",
    "minor_unit": "2",
    "name": "Canadian dollar"
  },
  {
    "code": "CDF",
    "numeric_code": "976",
    "minor_unit": "2",
    "name": "Congolese franc"
  },
  {
    "code": "CHE",
    "numeric_code": "947",
    "minor_unit": "2",
    "name": "WIR euro (complementary currency)"
  },
  {
    "code": "CHF",
    "numeric_code": "756",
    "minor_unit": "2",
    "name": "Swiss franc"
  },
  {
    "code": "CHW",
    "numeric_code": "948",
    "minor_unit": "2",
    "name": "WIR franc (complementary currency)"
  },
  {
    "code": "CLF",
    "numeric_code": "990",
    "minor_unit": "4",
    "name": "Unidad de Fomento (funds code)"
  },
  {
    "code": "CLP",
    "numeric_code": "152",
    "minor_unit": "0",
    "name": "Chilean peso"
  },
  {
    "code": "CNY",
    "numeric_code": "156",
    "minor_unit": "2",
    "name": "Chinese yuan[8]"
  },
  {
    "code": "COP",
    "numeric_code": "170",
    "minor_unit": "2",
    "name": "Colombian peso"
  },
  {
    "code": "COU",
    "numeric_code": "970",
    "minor_unit": "2[9]",
    "name": "Unidad de Valor Real (UVR) (funds code)[9]"
  },
  {
    "code": "CRC",
    "numeric_code": "188",
    "minor_unit": "2",
    "name": "Costa Rican colon"
  },
  {
    "code": "CUC",
    "numeric_code": "931",
    "minor_unit": "2",
    "name": "Cuban convertible peso"
  },
  {
    "code": "CUP",
    "numeric_code": "192",
    "minor_unit": "2",
    "name": "Cuban peso"
  },
  {
    "code": "CVE",
    "numeric_code": "132",
    "minor_unit": "2",
    "name": "Cape Verdean escudo"
  },
  {
    "code": "CZK",
    "numeric_code": "203",
    "minor_unit": "2",
    "name": "Czech koruna"
  },
  {
    "code": "DJF",
    "numeric_code": "262",
    "minor_unit": "0",
    "name": "Djiboutian franc"
  },
  {
    "code": "DKK",
    "numeric_code": "208",
    "minor_unit": "2",
    "name": "Danish krone"
  },
  {
    "code": "DOP",
    "numeric_code": "214",
    "minor_unit": "2",
    "name": "Dominican peso"
  },
  {
    "code": "DZD",
    "numeric_code": "12",
    "minor_unit": "2",
    "name": "Algerian dinar"
  },
  {
    "code": "EGP",
    "numeric_code": "818",
    "minor_unit": "2",
    "name": "Egyptian pound"
  },
  {
    "code": "ERN",
    "numeric_code": "232",
    "minor_unit": "2",
    "name": "Eritrean nakfa"
  },
  {
    "code": "ETB",
    "numeric_code": "230",
    "minor_unit": "2",
    "name": "Ethiopian birr"
  },
  {"code": "EUR", "numeric_code": "978", "minor_unit": "2", "name": "Euro"},
  {
    "code": "FJD",
    "numeric_code": "242",
    "minor_unit": "2",
    "name": "Fiji dollar"
  },
  {
    "code": "FKP",
    "numeric_code": "238",
    "minor_unit": "2",
    "name": "Falkland Islands pound"
  },
  {
    "code": "GBP",
    "numeric_code": "826",
    "minor_unit": "2",
    "name": "Pound sterling"
  },
  {
    "code": "GEL",
    "numeric_code": "981",
    "minor_unit": "2",
    "name": "Georgian lari"
  },
  {
    "code": "GHS",
    "numeric_code": "936",
    "minor_unit": "2",
    "name": "Ghanaian cedi"
  },
  {
    "code": "GIP",
    "numeric_code": "292",
    "minor_unit": "2",
    "name": "Gibraltar pound"
  },
  {
    "code": "GMD",
    "numeric_code": "270",
    "minor_unit": "2",
    "name": "Gambian dalasi"
  },
  {
    "code": "GNF",
    "numeric_code": "324",
    "minor_unit": "0",
    "name": "Guinean franc"
  },
  {
    "code": "GTQ",
    "numeric_code": "320",
    "minor_unit": "2",
    "name": "Guatemalan quetzal"
  },
  {
    "code": "GYD",
    "numeric_code": "328",
    "minor_unit": "2",
    "name": "Guyanese dollar"
  },
  {
    "code": "HKD",
    "numeric_code": "344",
    "minor_unit": "2",
    "name": "Hong Kong dollar"
  },
  {
    "code": "HNL",
    "numeric_code": "340",
    "minor_unit": "2",
    "name": "Honduran lempira"
  },
  {
    "code": "HRK",
    "numeric_code": "191",
    "minor_unit": "2",
    "name": "Croatian kuna"
  },
  {
    "code": "HTG",
    "numeric_code": "332",
    "minor_unit": "2",
    "name": "Haitian gourde"
  },
  {
    "code": "HUF",
    "numeric_code": "348",
    "minor_unit": "2",
    "name": "Hungarian forint"
  },
  {
    "code": "IDR",
    "numeric_code": "360",
    "minor_unit": "2",
    "name": "Indonesian rupiah"
  },
  {
    "code": "ILS",
    "numeric_code": "376",
    "minor_unit": "2",
    "name": "Israeli new shekel"
  },
  {
    "code": "INR",
    "numeric_code": "356",
    "minor_unit": "2",
    "name": "Indian rupee"
  },
  {
    "code": "IQD",
    "numeric_code": "368",
    "minor_unit": "3",
    "name": "Iraqi dinar"
  },
  {
    "code": "IRR",
    "numeric_code": "364",
    "minor_unit": "2",
    "name": "Iranian rial"
  },
  {
    "code": "ISK",
    "numeric_code": "352",
    "minor_unit": "0",
    "name": "Icelandic króna"
  },
  {
    "code": "JMD",
    "numeric_code": "388",
    "minor_unit": "2",
    "name": "Jamaican dollar"
  },
  {
    "code": "JOD",
    "numeric_code": "400",
    "minor_unit": "3",
    "name": "Jordanian dinar"
  },
  {
    "code": "JPY",
    "numeric_code": "392",
    "minor_unit": "0",
    "name": "Japanese yen"
  },
  {
    "code": "KES",
    "numeric_code": "404",
    "minor_unit": "2",
    "name": "Kenyan shilling"
  },
  {
    "code": "KGS",
    "numeric_code": "417",
    "minor_unit": "2",
    "name": "Kyrgyzstani som"
  },
  {
    "code": "KHR",
    "numeric_code": "116",
    "minor_unit": "2",
    "name": "Cambodian riel"
  },
  {
    "code": "KMF",
    "numeric_code": "174",
    "minor_unit": "0",
    "name": "Comoro franc"
  },
  {
    "code": "KPW",
    "numeric_code": "408",
    "minor_unit": "2",
    "name": "North Korean won"
  },
  {
    "code": "KRW",
    "numeric_code": "410",
    "minor_unit": "0",
    "name": "South Korean won"
  },
  {
    "code": "KWD",
    "numeric_code": "414",
    "minor_unit": "3",
    "name": "Kuwaiti dinar"
  },
  {
    "code": "KYD",
    "numeric_code": "136",
    "minor_unit": "2",
    "name": "Cayman Islands dollar"
  },
  {
    "code": "KZT",
    "numeric_code": "398",
    "minor_unit": "2",
    "name": "Kazakhstani tenge"
  },
  {"code": "LAK", "numeric_code": "418", "minor_unit": "2", "name": "Lao kip"},
  {
    "code": "LBP",
    "numeric_code": "422",
    "minor_unit": "2",
    "name": "Lebanese pound"
  },
  {
    "code": "LKR",
    "numeric_code": "144",
    "minor_unit": "2",
    "name": "Sri Lankan rupee"
  },
  {
    "code": "LRD",
    "numeric_code": "430",
    "minor_unit": "2",
    "name": "Liberian dollar"
  },
  {
    "code": "LSL",
    "numeric_code": "426",
    "minor_unit": "2",
    "name": "Lesotho loti"
  },
  {
    "code": "LYD",
    "numeric_code": "434",
    "minor_unit": "3",
    "name": "Libyan dinar"
  },
  {
    "code": "MAD",
    "numeric_code": "504",
    "minor_unit": "2",
    "name": "Moroccan dirham"
  },
  {
    "code": "MDL",
    "numeric_code": "498",
    "minor_unit": "2",
    "name": "Moldovan leu"
  },
  {
    "code": "MGA",
    "numeric_code": "969",
    "minor_unit": "2[c]",
    "name": "Malagasy ariary"
  },
  {
    "code": "MKD",
    "numeric_code": "807",
    "minor_unit": "2",
    "name": "Macedonian denar"
  },
  {
    "code": "MMK",
    "numeric_code": "104",
    "minor_unit": "2",
    "name": "Myanmar kyat"
  },
  {
    "code": "MNT",
    "numeric_code": "496",
    "minor_unit": "2",
    "name": "Mongolian tögrög"
  },
  {
    "code": "MOP",
    "numeric_code": "446",
    "minor_unit": "2",
    "name": "Macanese pataca"
  },
  {
    "code": "MRU[11]",
    "numeric_code": "929",
    "minor_unit": "2[c]",
    "name": "Mauritanian ouguiya"
  },
  {
    "code": "MUR",
    "numeric_code": "480",
    "minor_unit": "2",
    "name": "Mauritian rupee"
  },
  {
    "code": "MVR",
    "numeric_code": "462",
    "minor_unit": "2",
    "name": "Maldivian rufiyaa"
  },
  {
    "code": "MWK",
    "numeric_code": "454",
    "minor_unit": "2",
    "name": "Malawian kwacha"
  },
  {
    "code": "MXN",
    "numeric_code": "484",
    "minor_unit": "2",
    "name": "Mexican peso"
  },
  {
    "code": "MXV",
    "numeric_code": "979",
    "minor_unit": "2",
    "name": "Mexican Unidad de Inversion (UDI) (funds code)"
  },
  {
    "code": "MYR",
    "numeric_code": "458",
    "minor_unit": "2",
    "name": "Malaysian ringgit"
  },
  {
    "code": "MZN",
    "numeric_code": "943",
    "minor_unit": "2",
    "name": "Mozambican metical"
  },
  {
    "code": "NAD",
    "numeric_code": "516",
    "minor_unit": "2",
    "name": "Namibian dollar"
  },
  {
    "code": "NGN",
    "numeric_code": "566",
    "minor_unit": "2",
    "name": "Nigerian naira"
  },
  {
    "code": "NIO",
    "numeric_code": "558",
    "minor_unit": "2",
    "name": "Nicaraguan córdoba"
  },
  {
    "code": "NOK",
    "numeric_code": "578",
    "minor_unit": "2",
    "name": "Norwegian krone"
  },
  {
    "code": "NPR",
    "numeric_code": "524",
    "minor_unit": "2",
    "name": "Nepalese rupee"
  },
  {
    "code": "NZD",
    "numeric_code": "554",
    "minor_unit": "2",
    "name": "New Zealand dollar"
  },
  {
    "code": "OMR",
    "numeric_code": "512",
    "minor_unit": "3",
    "name": "Omani rial"
  },
  {
    "code": "PAB",
    "numeric_code": "590",
    "minor_unit": "2",
    "name": "Panamanian balboa"
  },
  {
    "code": "PEN",
    "numeric_code": "604",
    "minor_unit": "2",
    "name": "Peruvian sol"
  },
  {
    "code": "PGK",
    "numeric_code": "598",
    "minor_unit": "2",
    "name": "Papua New Guinean kina"
  },
  {
    "code": "PHP",
    "numeric_code": "608",
    "minor_unit": "2",
    "name": "Philippine peso[12]"
  },
  {
    "code": "PKR",
    "numeric_code": "586",
    "minor_unit": "2",
    "name": "Pakistani rupee"
  },
  {
    "code": "PLN",
    "numeric_code": "985",
    "minor_unit": "2",
    "name": "Polish złoty"
  },
  {
    "code": "PYG",
    "numeric_code": "600",
    "minor_unit": "0",
    "name": "Paraguayan guaraní"
  },
  {
    "code": "QAR",
    "numeric_code": "634",
    "minor_unit": "2",
    "name": "Qatari riyal"
  },
  {
    "code": "RON",
    "numeric_code": "946",
    "minor_unit": "2",
    "name": "Romanian leu"
  },
  {
    "code": "RSD",
    "numeric_code": "941",
    "minor_unit": "2",
    "name": "Serbian dinar"
  },
  {
    "code": "RUB",
    "numeric_code": "643",
    "minor_unit": "2",
    "name": "Russian ruble"
  },
  {
    "code": "RWF",
    "numeric_code": "646",
    "minor_unit": "0",
    "name": "Rwandan franc"
  },
  {
    "code": "SAR",
    "numeric_code": "682",
    "minor_unit": "2",
    "name": "Saudi riyal"
  },
  {
    "code": "SBD",
    "numeric_code": "90",
    "minor_unit": "2",
    "name": "Solomon Islands dollar"
  },
  {
    "code": "SCR",
    "numeric_code": "690",
    "minor_unit": "2",
    "name": "Seychelles rupee"
  },
  {
    "code": "SDG",
    "numeric_code": "938",
    "minor_unit": "2",
    "name": "Sudanese pound"
  },
  {
    "code": "SEK",
    "numeric_code": "752",
    "minor_unit": "2",
    "name": "Swedish krona/kronor"
  },
  {
    "code": "SGD",
    "numeric_code": "702",
    "minor_unit": "2",
    "name": "Singapore dollar"
  },
  {
    "code": "SHP",
    "numeric_code": "654",
    "minor_unit": "2",
    "name": "Saint Helena pound"
  },
  {
    "code": "SLL",
    "numeric_code": "694",
    "minor_unit": "2",
    "name": "Sierra Leonean leone"
  },
  {
    "code": "SOS",
    "numeric_code": "706",
    "minor_unit": "2",
    "name": "Somali shilling"
  },
  {
    "code": "SRD",
    "numeric_code": "968",
    "minor_unit": "2",
    "name": "Surinamese dollar"
  },
  {
    "code": "SSP",
    "numeric_code": "728",
    "minor_unit": "2",
    "name": "South Sudanese pound"
  },
  {
    "code": "STN[13]",
    "numeric_code": "930",
    "minor_unit": "2",
    "name": "São Tomé and Príncipe dobra"
  },
  {
    "code": "SVC",
    "numeric_code": "222",
    "minor_unit": "2",
    "name": "Salvadoran colón"
  },
  {
    "code": "SYP",
    "numeric_code": "760",
    "minor_unit": "2",
    "name": "Syrian pound"
  },
  {
    "code": "SZL",
    "numeric_code": "748",
    "minor_unit": "2",
    "name": "Swazi lilangeni"
  },
  {
    "code": "THB",
    "numeric_code": "764",
    "minor_unit": "2",
    "name": "Thai baht"
  },
  {
    "code": "TJS",
    "numeric_code": "972",
    "minor_unit": "2",
    "name": "Tajikistani somoni"
  },
  {
    "code": "TMT",
    "numeric_code": "934",
    "minor_unit": "2",
    "name": "Turkmenistan manat"
  },
  {
    "code": "TND",
    "numeric_code": "788",
    "minor_unit": "3",
    "name": "Tunisian dinar"
  },
  {
    "code": "TOP",
    "numeric_code": "776",
    "minor_unit": "2",
    "name": "Tongan paʻanga"
  },
  {
    "code": "TRY",
    "numeric_code": "949",
    "minor_unit": "2",
    "name": "Turkish lira"
  },
  {
    "code": "TTD",
    "numeric_code": "780",
    "minor_unit": "2",
    "name": "Trinidad and Tobago dollar"
  },
  {
    "code": "TWD",
    "numeric_code": "901",
    "minor_unit": "2",
    "name": "New Taiwan dollar"
  },
  {
    "code": "TZS",
    "numeric_code": "834",
    "minor_unit": "2",
    "name": "Tanzanian shilling"
  },
  {
    "code": "UAH",
    "numeric_code": "980",
    "minor_unit": "2",
    "name": "Ukrainian hryvnia"
  },
  {
    "code": "UGX",
    "numeric_code": "800",
    "minor_unit": "0",
    "name": "Ugandan shilling"
  },
  {
    "code": "USD",
    "numeric_code": "840",
    "minor_unit": "2",
    "name": "United States dollar"
  },
  {
    "code": "USN",
    "numeric_code": "997",
    "minor_unit": "2",
    "name": "United States dollar (next day) (funds code)"
  },
  {
    "code": "UYI",
    "numeric_code": "940",
    "minor_unit": "0",
    "name": "Uruguay Peso en Unidades Indexadas (URUIURUI) (funds code)"
  },
  {
    "code": "UYU",
    "numeric_code": "858",
    "minor_unit": "2",
    "name": "Uruguayan peso"
  },
  {
    "code": "UYW",
    "numeric_code": "927",
    "minor_unit": "4",
    "name": "Unidad previsional[14]"
  },
  {
    "code": "UZS",
    "numeric_code": "860",
    "minor_unit": "2",
    "name": "Uzbekistan som"
  },
  {
    "code": "VES",
    "numeric_code": "928",
    "minor_unit": "2",
    "name": "Venezuelan bolívar soberano[12]"
  },
  {
    "code": "VND",
    "numeric_code": "704",
    "minor_unit": "0",
    "name": "Vietnamese đồng"
  },
  {
    "code": "VUV",
    "numeric_code": "548",
    "minor_unit": "0",
    "name": "Vanuatu vatu"
  },
  {
    "code": "WST",
    "numeric_code": "882",
    "minor_unit": "2",
    "name": "Samoan tala"
  },
  {
    "code": "XAF",
    "numeric_code": "950",
    "minor_unit": "0",
    "name": "CFA franc BEAC"
  },
  {
    "code": "XAG",
    "numeric_code": "961",
    "minor_unit": ".",
    "name": "Silver (one troy ounce)"
  },
  {
    "code": "XAU",
    "numeric_code": "959",
    "minor_unit": ".",
    "name": "Gold (one troy ounce)"
  },
  {
    "code": "XBA",
    "numeric_code": "955",
    "minor_unit": ".",
    "name": "European Composite Unit (EURCO) (bond market unit)"
  },
  {
    "code": "XBB",
    "numeric_code": "956",
    "minor_unit": ".",
    "name": "European Monetary Unit (E.M.U.-6) (bond market unit)"
  },
  {
    "code": "XBC",
    "numeric_code": "957",
    "minor_unit": ".",
    "name": "European Unit of Account 9 (E.U.A.-9) (bond market unit)"
  },
  {
    "code": "XBD",
    "numeric_code": "958",
    "minor_unit": ".",
    "name": "European Unit of Account 17 (E.U.A.-17) (bond market unit)"
  },
  {
    "code": "XCD",
    "numeric_code": "951",
    "minor_unit": "2",
    "name": "East Caribbean dollar"
  },
  {
    "code": "XDR",
    "numeric_code": "960",
    "minor_unit": ".",
    "name": "Special drawing rights"
  },
  {
    "code": "XOF",
    "numeric_code": "952",
    "minor_unit": "0",
    "name": "CFA franc BCEAO"
  },
  {
    "code": "XPD",
    "numeric_code": "964",
    "minor_unit": ".",
    "name": "Palladium (one troy ounce)"
  },
  {
    "code": "XPF",
    "numeric_code": "953",
    "minor_unit": "0",
    "name": "CFP franc (franc Pacifique)"
  },
  {
    "code": "XPT",
    "numeric_code": "962",
    "minor_unit": ".",
    "name": "Platinum (one troy ounce)"
  },
  {"code": "XSU", "numeric_code": "994", "minor_unit": ".", "name": "SUCRE"},
  {
    "code": "XTS",
    "numeric_code": "963",
    "minor_unit": ".",
    "name": "Code reserved for testing"
  },
  {
    "code": "XUA",
    "numeric_code": "965",
    "minor_unit": ".",
    "name": "ADB Unit of Account"
  },
  {
    "code": "XXX",
    "numeric_code": "999",
    "minor_unit": ".",
    "name": "No currency"
  },
  {
    "code": "YER",
    "numeric_code": "886",
    "minor_unit": "2",
    "name": "Yemeni rial"
  },
  {
    "code": "ZAR",
    "numeric_code": "710",
    "minor_unit": "2",
    "name": "South African rand"
  },
  {
    "code": "ZMW",
    "numeric_code": "967",
    "minor_unit": "2",
    "name": "Zambian kwacha"
  },
  {
    "code": "ZWL",
    "numeric_code": "932",
    "minor_unit": "2",
    "name": "Zimbabwean dollar"
  },
];
