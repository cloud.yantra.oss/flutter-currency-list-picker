/// Currency element. This is the element that contains all the information
class CurrencyCode {
  /// the name of the currency
  String name;

  /// the currency code (EUR,INR,etc)
  String code;

  /// the numeric code for the currency
  String numericCode;

  /// the numeric code for the currency
  String minorUnit;

  /// the symbol of the currency
  String symbol;

  CurrencyCode(
      {required this.code,
      required this.numericCode,
      required this.minorUnit,
      required this.name,
      required this.symbol});

  @override
  String toString() => "$code";

  String toLongString() => "$numericCode $name";

  String toCurrencyStringOnly() => '$name';
}
