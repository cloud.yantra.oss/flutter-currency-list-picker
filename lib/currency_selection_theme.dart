import 'package:flutter/material.dart';

class CurrencyListPickerTheme {
  final String searchText;
  final String searchHintText;
  final String lastPickText;
  final Color alphabetSelectedBackgroundColor;
  final Color alphabetTextColor;
  final Color alphabetSelectedTextColor;
  final bool isShowTitle;
  final bool isShowSymbol;
  final bool isShowCode;
  final bool isDownIcon;
  final String initialSelection;
  final bool showEnglishName;

  CurrencyListPickerTheme({
    this.searchText = "",
    this.searchHintText = "",
    this.lastPickText = "",
    this.alphabetSelectedBackgroundColor = Colors.transparent,
    this.alphabetTextColor = Colors.black,
    this.alphabetSelectedTextColor = Colors.grey,
    this.isShowTitle = true,
    this.isShowSymbol = true,
    this.isShowCode = true,
    this.isDownIcon = true,
    this.initialSelection = "EUR",
    this.showEnglishName = true,
  });
}
