import 'dart:async';

import 'package:currencylistpicker/models/currency_codes_en.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'currency_selection_theme.dart';
import 'models/currency_codes.dart';
import 'models/currency_model.dart';
import 'selection_list.dart';

class CurrencyListPicker extends StatefulWidget {
  static const MethodChannel _channel =
      const MethodChannel('currencylistpicker');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  CurrencyListPicker(
      {required this.onChanged,
      this.appBar,
      this.initialSelection,
      this.builder,
      this.selectionBuilder,
      this.theme,
      this.useUiOverlay = true,
      this.useSafeArea = false});

  final ValueChanged<CurrencyCode> onChanged;
  final String? initialSelection;
  final PreferredSizeWidget? appBar;
  final CurrencyListPickerTheme? theme;
  final Widget Function(BuildContext context, CurrencyCode code)? builder;
  final Widget Function(BuildContext context, CurrencyCode code)?
      selectionBuilder;
  final bool useUiOverlay;
  final bool useSafeArea;

  final NumberFormat numberFormat = NumberFormat();

  @override
  _CurrencyListPickerState createState() {
    //TODO: Multi-language support
    List<Map> jsonList = this.theme?.showEnglishName ?? true ? codes_en : codes;

    List elements = jsonList
        .map((s) => CurrencyCode(
              name: s['name'],
              code: s['code'],
              numericCode: s['numeric_code'],
              minorUnit: s['minor_unit'],
              symbol: numberFormat.simpleCurrencySymbol(s['code']),
            ))
        .toList();
    return _CurrencyListPickerState(elements);
  }
}

class _CurrencyListPickerState extends State<CurrencyListPicker> {
  late CurrencyCode selectedItem;
  List elements = [];

  _CurrencyListPickerState(this.elements);

  @override
  void initState() {
    if (widget.initialSelection != null) {
      selectedItem = elements.firstWhere(
          (e) =>
              (e.code.toUpperCase() == widget.initialSelection!.toUpperCase()),
          orElse: () => elements[0] as CurrencyCode);
    } else {
      selectedItem = elements[0];
    }

    super.initState();
  }

  void _awaitFromSelectScreen(BuildContext context, PreferredSizeWidget? appBar,
      CurrencyListPickerTheme? theme) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SelectionList(
            elements,
            selectedItem,
            appBar: widget.appBar ??
                AppBar(
                  backgroundColor: Theme.of(context).appBarTheme.color,
                  title: Text("Select Currency"),
                ),
            theme: theme,
            builder: widget.selectionBuilder,
            useUiOverlay: widget.useUiOverlay,
            useSafeArea: widget.useSafeArea,
          ),
        ));

    setState(() {
      selectedItem = result ?? selectedItem;
      widget.onChanged(result ?? selectedItem);
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {
        _awaitFromSelectScreen(context, widget.appBar, widget.theme);
      },
      child: widget.builder != null
          ? widget.builder!(context, selectedItem)
          : Flex(
              mainAxisAlignment: MainAxisAlignment.center,
              direction: Axis.horizontal,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (widget.theme?.isShowSymbol ?? true)
                  Flexible(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(selectedItem.symbol,
                          style: Theme.of(context).textTheme.subtitle1),
                    ),
                  ),
                if (widget.theme?.isShowCode ?? true)
                  Flexible(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(selectedItem.code,
                          style: Theme.of(context).textTheme.subtitle1),
                    ),
                  ),
                if (widget.theme?.isShowTitle ?? true)
                  Flexible(
                    flex: 4,
                    fit: FlexFit.loose,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(selectedItem.name,
                          style: Theme.of(context).textTheme.subtitle1),
                    ),
                  ),
                if (widget.theme?.isDownIcon ?? true)
                  Flexible(
                    flex: 1,
                    child: Icon(Icons.arrow_drop_down_rounded),
                  )
              ],
            ),
    );
  }
}
