# Currency List Picker

Flutter plugin project to pick currency code, will show currency name and ISO currency codes.

## Usage

To use this plugin, add `currencylistpicker` as a [dependency in your pubspec.yaml](https://flutter.io/platform-plugins/).

```dart
    CurrencyListPicker(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text('Choose your currency'),
        ),
        
        // if you need custome picker use this
        pickerBuilder: (context, CurrencyCode code){
          return Row(
            children: [
              Text(code.code),
              Text(code.name),
            ],
          );
         },

        // To disable option set to false
        theme: CurrencyListPickerTheme(
            isShowSymbol: true,
            isShowTitle: true,
            isShowCode: true,
            isDownIcon: true,
            showEnglishName: true,
            ),
        // Set default currency code
        initialSelection: 'EUR',
        onChanged: (CurrencyCode code) {
          print(code.name);
          print(code.code);
          print(code.numericCode);
          print(code.minorUnit);
          print(code.symbol);
        },
        // Whether to allow the widget to set a custom UI overlay
        useUiOverlay: true,
        // Whether the currency list should be wrapped in a SafeArea
        useSafeArea: false
        ),
```

To call feedback or getting data from this widget, you can make function in onChanged


### Special Thanks
I thank contributors of [CountryListPick](https://github.com/hifiaz/country-list-pick), which was used as reference while developing this library.
